package com.empik.restfulapiproject.service;

import org.springframework.stereotype.Service;

import com.empik.restfulapiproject.dto.UserGithubInfoRequest;
import com.empik.restfulapiproject.dto.UserGithubInfoResponse;
import com.empik.restfulapiproject.exception.ExceptionCode;
import com.empik.restfulapiproject.exception.RestApiException;
import com.empik.restfulapiproject.mapper.UserInfoMapper;
import com.empik.restfulapiproject.model.User;
import com.empik.restfulapiproject.repository.UserRepository;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class UserService {

	private final GithubApiClient githubApiClient;
	private final UserRepository userRepository;
	private final UserInfoMapper userInfoMapper;

	public UserGithubInfoResponse getUserInfoByGithubLogin(String login) {

		UserGithubInfoRequest userGithubInfoRequest = githubApiClient.getUserInfoByGithubLogin(login);

		User foundUser = userRepository.findByLogin(userGithubInfoRequest.getLogin()).map(user -> {
			user.setRequestCounter(user.getRequestCounter() + 1);
			return user;

		}).orElse(new User(userGithubInfoRequest.getLogin(), 1));

		double calculations = calculations(userGithubInfoRequest);
		
		userRepository.save(foundUser);

		return userInfoMapper.userRequestToResponseBuilder(userGithubInfoRequest, String.valueOf(calculations));
	}

	private double calculations(UserGithubInfoRequest userGithubInfoRequest) {

		if (userGithubInfoRequest.getNumberOfFollowers() == 0) {

			throw new RestApiException(ExceptionCode.ILLEGAL_CALCULATION_ARGUMENT);
		}

		return ((6 / userGithubInfoRequest.getNumberOfFollowers())
				* (2 + userGithubInfoRequest.getNumberOfPublicRepositories()));
	}

}
