package com.empik.restfulapiproject.service;

import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClient.Builder;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import com.empik.restfulapiproject.dto.UserGithubInfoRequest;
import com.empik.restfulapiproject.exception.ExceptionCode;
import com.empik.restfulapiproject.exception.RestApiException;

@Component
public class GithubApiClient {

	private final static String GITHUB_API_URL = "https://api.github.com";
	private final static String GITHUB_USER_API = "%s/users/%s";

	private final WebClient.Builder webClientBuilder;

	public GithubApiClient(Builder webClientBuilder) {
		this.webClientBuilder = webClientBuilder;
	}

	public UserGithubInfoRequest getUserInfoByGithubLogin(String login) {

		String url = String.format(GITHUB_USER_API, GITHUB_API_URL, login);

		UserGithubInfoRequest githubUserInfo = null;
		
		try {
		 githubUserInfo = webClientBuilder
				.build()
				.get()
				.uri(url)
				.retrieve()
				.bodyToMono(UserGithubInfoRequest.class)
				.block();
		}
		catch(WebClientResponseException exception) {
			throw new RestApiException(ExceptionCode.GITHUB_USER_NOT_FOUND);
		}
		return githubUserInfo;

	}

}
