package com.empik.restfulapiproject.exception;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class RestApiExceptionResponse {
	
	 final String message;
	 final int httpStatus;
	

}
