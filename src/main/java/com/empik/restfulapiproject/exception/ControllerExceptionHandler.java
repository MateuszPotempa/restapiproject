package com.empik.restfulapiproject.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(RestApiException.class)
	public ResponseEntity<RestApiExceptionResponse> resourceNotFoundException(RestApiException ex) {
		RestApiExceptionResponse response = RestApiExceptionResponse.builder()
				.message(ex.getMessage())
				.httpStatus(ex.httpStatus.value())
				.build();

		return new ResponseEntity<RestApiExceptionResponse>(response, ex.httpStatus);
	}

}
