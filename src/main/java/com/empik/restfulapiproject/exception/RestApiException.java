package com.empik.restfulapiproject.exception;

import org.springframework.http.HttpStatus;

public class RestApiException extends RuntimeException {

	final HttpStatus httpStatus;
	
	
	public RestApiException(ExceptionCode exceptionCode) {
		super(exceptionCode.message);
		this.httpStatus=exceptionCode.httpStatus;
	}
	
	
	

}
