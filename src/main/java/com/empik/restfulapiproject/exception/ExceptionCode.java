package com.empik.restfulapiproject.exception;

import org.springframework.http.HttpStatus;

public enum ExceptionCode {

	GITHUB_USER_NOT_FOUND("Github user with given login do not exist", HttpStatus.NOT_FOUND),
	
	ILLEGAL_CALCULATION_ARGUMENT("Followers number for the user is equal to 0. Calculations can't be proceeded.",HttpStatus.NOT_ACCEPTABLE);

	final String message;
	final HttpStatus httpStatus;

	private ExceptionCode(String message, HttpStatus httpStatus) {
		this.message = message;
		this.httpStatus = httpStatus;
	}

}
