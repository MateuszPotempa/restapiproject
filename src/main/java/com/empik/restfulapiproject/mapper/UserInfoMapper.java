package com.empik.restfulapiproject.mapper;

import java.util.Objects;

import org.springframework.stereotype.Component;

import com.empik.restfulapiproject.dto.UserGithubInfoRequest;
import com.empik.restfulapiproject.dto.UserGithubInfoResponse;

@Component
public class UserInfoMapper {

	public UserGithubInfoResponse userRequestToResponseBuilder(UserGithubInfoRequest userGithubInfoRequest, String calculation) {
		
		return UserGithubInfoResponse.builder()
				.id(userGithubInfoRequest.getId())
				.login(userGithubInfoRequest.getLogin())
				.name(Objects.nonNull(userGithubInfoRequest.getName())?userGithubInfoRequest.getName() : "")
				.type(userGithubInfoRequest.getType())
				.avatarUrl(userGithubInfoRequest.getAvatarUrl())
				.createdAt(userGithubInfoRequest.getCreatedAt())
				.calculations(calculation)
				.build();
	}
	
}
