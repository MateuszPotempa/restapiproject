package com.empik.restfulapiproject.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserGithubInfoResponse {

	private int id;
	private String login;
	private String name;
	private String type;
	@JsonProperty("avatar_url")
	private String avatarUrl;
	@JsonProperty("created_at")
	private String createdAt;
	private String calculations;

}
