package com.empik.restfulapiproject.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserGithubInfoRequest {

	private int id;
	private String login;
	private String name;
	private String type;
	@JsonProperty("avatar_url")
	private String avatarUrl;
	@JsonProperty("created_at")
	private String createdAt;
	@JsonProperty("followers")
	private double numberOfFollowers;
	@JsonProperty("public_repos")
	private double numberOfPublicRepositories;

}
