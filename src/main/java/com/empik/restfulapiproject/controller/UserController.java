package com.empik.restfulapiproject.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.empik.restfulapiproject.dto.UserGithubInfoResponse;
import com.empik.restfulapiproject.service.UserService;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping(path = "/users/")
@AllArgsConstructor
public class UserController {

	private final UserService userService;

	@GetMapping(path = "{login}", produces = MediaType.APPLICATION_JSON_VALUE)
	public UserGithubInfoResponse getUserInfoByGithubLogin(@PathVariable("login") String githubLogin) {

		return userService.getUserInfoByGithubLogin(githubLogin);

	}
}
