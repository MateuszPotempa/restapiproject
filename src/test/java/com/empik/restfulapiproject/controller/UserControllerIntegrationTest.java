package com.empik.restfulapiproject.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.empik.restfulapiproject.dto.UserGithubInfoResponse;
import com.empik.restfulapiproject.exception.ExceptionCode;
import com.empik.restfulapiproject.exception.RestApiException;
import com.empik.restfulapiproject.service.UserService;


@WebMvcTest(controllers = UserController.class)
class UserControllerIntegrationTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private UserService userService;

	@Test
	void should_Return_GithubUser_ByLogin() throws Exception {

		final UserGithubInfoResponse userResponse = UserGithubInfoResponse.builder().id(383316).name("").login("test")
				.avatarUrl("https://avatars.githubusercontent.com/u/383316?v=4").createdAt("2010-09-01T10:39:12Z")
				.type("User").calculations("0.875").build();

		given(this.userService.getUserInfoByGithubLogin("test")).willReturn(userResponse);

		this.mockMvc.perform(get("/users/test")).andExpect(status().isOk())
				.andExpect(jsonPath("$.id", is(userResponse.getId())))
				.andExpect(jsonPath("$.name", is(userResponse.getName())))
				.andExpect(jsonPath("$.login", is(userResponse.getLogin())))
				.andExpect(jsonPath("$.avatar_url", is(userResponse.getAvatarUrl())))
				.andExpect(jsonPath("$.created_at", is(userResponse.getCreatedAt())))
				.andExpect(jsonPath("$.type", is(userResponse.getType())))
				.andExpect(jsonPath("$.calculations", is(userResponse.getCalculations())));

	}

	@Test
	void should_Return4xx_When_GithubUser_NotExist() throws Exception {

		doThrow(new RestApiException(ExceptionCode.GITHUB_USER_NOT_FOUND)).when(userService)
				.getUserInfoByGithubLogin(anyString());

		mockMvc.perform(get("/users/testUser")).andExpect(status().isNotFound());

	}

//	@Test
//	void should_Return2xx_When_GithubUser_Exists() throws Exception {
//		
//		//given
//		String expectedJson="{\"id\":383316,\"login\":\"test\",\"name\":\"\",\"type\":\"User\",\"calculations\":\"0.875\",\"avatar_url\":\"https://avatars.githubusercontent.com/u/383316?v=4\",\"created_at\":\"2010-09-01T10:39:12Z\"}";
//			
//		//then	
//		MvcResult result =this.mockMvc.perform(get("/users/test"))
//		.andExpect(status().isOk())
//		.andReturn();
//		
//		 String actualJson = result.getResponse().getContentAsString();
//		 
//		 assertEquals(expectedJson, actualJson);
//
//	}

}
